<?php
$indicador=str_replace('-','_', $views[0]);
date_default_timezone_set('UTC');
$fechaActual=date("Y-m-d");

if($views[0]!=null){
	if(isset($views[1])){
		$valor=json_decode(file_get_contents('https://mindicador.cl/api/'.$indicador));
		echo'<div class="cja1">';
		titulo(str_replace('-',' ', $views[0]));
		echo'	<div class="ctx2">';
		foreach($valor->serie as $item){
			echo'<div class="cja3 df js aic">';
			echo'	<p>'.verfecha(date("Y-m-d", strtotime($item->fecha))).'</p>';
			echo'	<p>'.$item->valor.'</p>';
			echo'</div>';
		}
		echo'	</div>';
		echo'</div>';
	}else{
		$valor=json_decode(file_get_contents('https://mindicador.cl/api/'.$indicador.'/'.verfecha($fechaActual)));
		echo'<div class="cja1">';
		titulo(str_replace('-',' ', $views[0]));
		echo'	<div class="ctx1 df fdc jc aic">';
		echo'		<p>'.$valor->serie[0]->valor.'</p>';
		echo'		<span>'.$valor->unidad_medida.'</span>';
		echo'		<a href="'.SERVERURL.'/uf/historico" class="btn2 df jc aic">';
		echo'			<p>Ver historico</p>';
		echo'		</a>';
		echo'	</div>';
		echo'</div>';
	}
}else{
	echo'<h2>Indices económicos al día de '.date("Y-m-d", strtotime($json->fecha)).'</h2>';
}

function titulo($titutlo){
	echo'<div class="ttl1 df jc aic"><h2>'.$titutlo.'</h2></div>';
}

function verfecha($vfecha){
	$fch=explode("-",$vfecha);
	$tfecha=$fch[2]."-".$fch[1]."-".$fch[0];
	return $tfecha;
}
?>