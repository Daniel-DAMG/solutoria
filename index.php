<?php
include 'lib/config.php';
$apiUrl='https://mindicador.cl/api';
if(ini_get('allow_url_fopen')){
	$json=json_decode(file_get_contents($apiUrl));
}else{
	$curl=curl_init($apiUrl);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$json=curl_exec($curl);
	curl_close($curl);
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="DC.Creator" content="Daniel Mardones">
	<meta name="robots" content="ALL">
	<meta name="Author" content="DAMG ofrece a nuestros clientes un servicio de excelencia, acompañándolos y asesorándolos en la incorporación de las empresas al mundo del desarrollo Web, interfaces web. www.damg.cl" lang="es">
	<meta name="keywords" content=""/>
	<title>Solutoria</title>
	<link rel="stylesheet" href="<?php echo SERVERURL; ?>css/damgStyle.css">
</head>
<body>
	<header>
		<div class="crp1">
			<div class="bra1 df jc aic">
				<a href="<?php echo SERVERURL; ?>"><h1>Listado de <b>Indicadores Económicos</b></h1></a>
			</div>
		</div>
	</header>
	<main>
	<?php
	include 'views/home-valores.php';
	//echo'<pre>';
	//print_r($json);
	//echo'</pre>';
	?>
	</main>
</body>
</html>
